class Api::V1::ItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :updated_at
end
