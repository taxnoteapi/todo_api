class Api::V1::UserSerializer < ActiveModel::Serializer
  attributes :id, :name 
  # has_many :items

  ## Set items display with Api::V1::ItemSerializer
  has_many :items, serializer: Api::V1::ItemSerializer 

end
