class Api::V1::ListSerializer < ActiveModel::Serializer
  attributes :id, :name, :uuid, :deleted, :created_at, :updated_at, :items #, :deleted_items

  def items
    filtered_items = filter_items(object.items)
    # ActiveModel::ArraySerializer.new(filtered_items, scope: {list: object}, earch_serializer: Api::V1::ItemSerializer)
  end

  # def deleted_items
  #   ActiveModel::ArraySerializer.new(object.items.deleted(true), scope: {list: object}, earch_serializer: Api::V1::ItemSerializer)
  # end

  private
    def filter_items(items)
      updated_at = options[:updated_at]

      # Filter with updated_at for refresh
      if updated_at.present?
        filtered_items = items.where('updated_at > ?', Time.at(updated_at.to_i))
      
      # Get not deleted ones for login
      else
      	filtered_items = items.where(deleted: false)
      end

      filtered_items
    end
end