module UuidTools
  extend ActiveSupport::Concern

  included do
    validates :uuid, uniqueness: true
    before_create :set_uuid
  end

  private

  def set_uuid
    self.uuid = SecureRandom.uuid if uuid.blank?
  end
  
end