class List < ActiveRecord::Base
  include UuidTools

  belongs_to :user
  has_many :items, dependent: :destroy 

  validates :name, :user, presence: true

  # Enable it later
  # validates :name, :uuid, :user, presence: true

  validate :uniqueness_of_name

  # scope :deleted, -> (val) { where(deleted: val) }
  # scope :active, -> (val) { where(deleted: !val) }

  def mark_all_related_objects_deleted!
    self.update_attribute(:deleted, true)
    self.items.update_all(deleted: true)
  end

  def uniqueness_of_name

    # if self.user && self.user.lists.where(name: self.name).exists?
    if self.user && self.user.lists.where(name: name).exists?
      errors.add('name', 'Should be unique')
      return false
    else
      return true
    end
  end

end
