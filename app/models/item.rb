class Item < ActiveRecord::Base
  include UuidTools
  
  belongs_to :list

  validates :name, presence: true
  
  scope :deleted, -> (val) { where(deleted: val) }

  # Enable it later
  # validates :name, :uuid, presence: true
end
