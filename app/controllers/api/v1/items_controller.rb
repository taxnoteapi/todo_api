class Api::V1::ItemsController < Api::V1::BaseController
  before_action :fetch_item, only: [:destroy, :update]

  # GET /items
  def index
    items      = current_user.items

    updated_at = params[:updated_at]
   
    # Filter with updated_at if updated_at exists, it should send deleted items too
    if updated_at.present?     
      items = items.where("updated_at > ?", Time.at(updated_at.to_i))

    # Get items which are not deleted after login from mobile
    else
      items = items.where(deleted: false)
    end

    render json: items
  end

  def show
    render json: @item
  end

  # POST /items
  def create
    item = Item.new

    item.name = item_params[:name];
    item.list = fetch_list;

    # Enable it later
    # item.uuid = item_params[:uuid];

    if item.save
      render json: item, status: :created
    else
      render json: {error:  item.errors }, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /items/1
  def update
    item = @item

    # Update list only if list_uuid param exists
    if item_params[:list_uuid].present?
      item.list = fetch_list
    end

    item.attributes = params.require(:item).permit(:name)

    if item.save
      head :no_content
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /items/1
  def destroy
    if @item.update(deleted: true)
      head :no_content
    else
      render json: item.errors, status: :unprocessable_entity
    end
  end


  private
    def fetch_list
      # Call exception if there is no match and error message gets called
      List.find_by!(uuid: item_params[:list_uuid])
    end

    def fetch_item
      # Fetch uuid from items/id
      uuid  = params[:id]
      @item = Item.find_by!(uuid: uuid)
    end

    def item_params
      params.require(:item).permit(:name, :uuid, :list_uuid)
    end
end