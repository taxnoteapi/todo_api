class Api::V1::UsersController < Api::V1::BaseController
  before_action :set_user, only: [:show, :update, :destroy, :get_user_lists, :get_user_items]

  # respond_to :json

  # GET /users
  # GET /users.json
  def index

    render json: User.all
    # render :json => User.all

    # render(
    #     root: false,
    #     json: User.all,
    #     # each_serializer: Api::V1::UserSerializer
    #   )
  end

  # GET /users/1
  # GET /users/1.json
  def show
    # render json: @user

      render(
          root: false,
          status: :ok,
          json: @user,
          serializer: Api::V1::UserSerializer
      )
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :password)
    end
end
