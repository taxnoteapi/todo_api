class Api::V1::ListsController < Api::V1::BaseController
  before_action :fetch_list, only: [:destroy, :update]

  # GET /lists
  def index
    lists       = current_user.lists
    updated_at  = params[:updated_at]
   
    # Filter with updated_at for refresh
    if updated_at.present?

      #QQ what is the better format for passing date from iPhone?
      lists = lists.where("updated_at > ?", Time.at(updated_at.to_i))

    # Get not deleted ones for login
    else
      lists = lists.where(deleted: false)
    end

    render(
      root: false,
      json: lists,
      updated_at: updated_at,
      each_serializer: Api::V1::ListSerializer
    )
  end

  def show
    render json: @list
  end

  # POST /lists
  def create
    list            = List.new
    list.user       = current_user
    list.attributes = list_params

    if list.save
      render json: list, status: :created
    else
      render json: list.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /lists/1
  def update
    list            = @list
    list.attributes = list_params

    if list.save
      head :no_content
    else
      render json: @list.errors, status: :unprocessable_entity
    end
  end

  # DELETE /lists/1
  def destroy
    @list.mark_all_related_objects_deleted!
    head :no_content
  end

  # DELETE /lists
  def destroy_all
    lists = current_user.lists
    lists.each do |list|
      list.mark_all_related_objects_deleted!
    end

    head :no_content
  end


  private
    def fetch_list
      uuid  = params[:id]
      @list = List.find_by!(uuid: uuid)
    end

    def list_params
      params.require(:list).permit(:name, :uuid)
    end
end