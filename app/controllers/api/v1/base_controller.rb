class Api::V1::BaseController < ApplicationController
  # before_action :authenticate_user!

  def current_user
    @current_user ||= User.first
  end

  def auth_user
    current_user
  end
end
