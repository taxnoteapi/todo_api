class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActionController::ParameterMissing, with: :invalid_params


  private

  def record_not_found

  # ActiveRecord::RecordNotFound (Couldn't find List) from console
    render json: {errors: {message: 'Record not found'}}
  end

  def invalid_params
    render json: {errors: {message: 'Params are invalid'}}
  end

end 
