class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :name
      t.references :user
      t.string :uuid
      t.boolean :deleted, default: false
      
      t.timestamps null: false
    end
  end
end
