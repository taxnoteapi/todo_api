class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.references :list   
      t.string :uuid
      t.boolean :deleted, default: false
      t.timestamps null: false
    end
  end
end
