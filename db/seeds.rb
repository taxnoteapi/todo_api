# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(email: 'email@email.com', password: 'validpassword', password_confirmation: 'validpassword')
list = List.create(name: 'list1', user: user)
list2 = List.create(name: 'list2', user: user)
Item.create(name:"firstItem", list: list)
Item.create(name:"secondItem", list: list)
Item.create(name:"thirdItem", list: list, deleted: true)
Item.create(name:"forthItem", list: list, deleted: true)
Item.create(name:"blue", list: list2)
Item.create(name:"read", list: list2)