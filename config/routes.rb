Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'api/auth'

  namespace :api do
      namespace :v1 do
        resources :users do
          member do
            get :get_user_lists
          end
          collection do
            get :get_user_items
          end
        end
        resources :items
        resources :lists do
          collection do
            delete :destroy_all
        end
      end
    end
  end
end
